<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_status', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('server_id')->unsigned();
            $table->boolean('is_online');
            $table->integer('current_players');
            $table->integer('max_players');
            $table->text('players');
            $table->timestamp('date');

            $table->foreign('server_id')->references('id')->on('servers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('server_status');
    }
}
