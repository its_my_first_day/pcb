<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreadLastPostId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forum_threads', function (Blueprint $table) {
            $table->integer('last_post_id')->unsigned()->nullable();

            $table->foreign('last_post_id')->references('id')->on('forum_posts');
        });

        Schema::table('forum_boards', function (Blueprint $table) {
            $table->integer('last_thread_id')->unsigned()->nullable();
            $table->integer('last_post_id')->unsigned()->nullable();

            $table->foreign('last_thread_id')->references('id')->on('forum_threads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forum_threads', function($table)
        {
            $table->dropForeign('forum_threads_last_post_id_foreign');

            $table->dropColumn(array('last_post_id'));
        });

        Schema::table('forum_boards', function($table)
        {
            $table->dropForeign('forum_boards_last_thread_id_foreign');

            $table->dropColumn(array('last_thread_id', 'last_post_id'));
        });
    }
}
