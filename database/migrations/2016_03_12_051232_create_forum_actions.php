<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_thread_actions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('thread_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('action', [
                'locked',
                'sealed',
                'stickied',
                'archived',
                'deleted'
            ]);
            $table->boolean('is_active');   // ie. 1 = locked, 0 = unlocked
            $table->timestamp('created_at');

            $table->foreign('thread_id')->references('id')->on('forum_threads');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forum_thread_actions');
    }
}
