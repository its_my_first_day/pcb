<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumBoards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_boards', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('cat_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('description');
            $table->string('thread_model');
            $table->integer('total_threads');
            $table->integer('total_posts');
            $table->integer('order');

            $table->foreign('cat_id')->references('id')->on('forum_categories');
            $table->foreign('parent_id')->references('id')->on('forum_boards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forum_boards');
    }
}
