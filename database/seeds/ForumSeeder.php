<?php

use Illuminate\Database\Seeder;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // board categories
        $cat1 = DB::table('forum_categories')->insert([
            'name'  => 'Category 1',
            'order' => 1
        ]);

        $cat2 = DB::table('forum_categories')->insert([
            'name'  => 'Category 2',
            'order' => 2
        ]);

        // boards
        $board1 = DB::table('forum_boards')->insert([
            'name'          => 'Normal Board',
            'description'   => 'A test board',
            'cat_id'        => $cat1,
            'order'         => 1,
            'thread_model'  => 'post'
        ]);

        $board2 = DB::table('forum_boards')->insert([
            'name'          => 'A child board',
            'description'   => 'A board of a normal board',
            'cat_id'        => $cat1,
            'parent_id'     => $board1,
            'thread_model'  => 'post'
        ]);

        $board3 = DB::table('forum_boards')->insert([
            'name'          => 'Announcements',
            'description'   => 'A test board',
            'cat_id'        => $cat1,
            'order'         => 2,
            'thread_model'  => 'announcement'
        ]);

        $board4 = DB::table('forum_boards')->insert([
            'name'          => 'Another normal board',
            'description'   => 'A test board',
            'cat_id'        => $cat2,
            'order'         => 1,
            'thread_model'  => 'post'
        ]);

        // settings
        $setting1 = DB::table('settings')->insert([
            'key'   => 'web.announcement_board',
            'value' => $board3
        ]);

        $setting2 = DB::table('settings')->insert([
            'key'   => 'web.read_storage_threshold',
            'value' => 21
        ]);
    }
}
