<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create some users (with fixed seed for testing)
        $faker = Faker\Factory::create();
        $faker->seed(1234);

        // create 1 admin account for testing
        $admin_user = DB::table('users')->insert([
            'username'  => 'admin',
            'email'     => $faker->freeEmail,
            'password'  => \Illuminate\Support\Facades\Hash::make('admin')
        ]);

        for ($i=0; $i < 25; $i++) {
            $user_ids[] = DB::table('users')->insert([
                'username'  => $faker->userName,
                'email'     => $faker->freeEmail,
                'password'  => \Illuminate\Support\Facades\Hash::make($faker->password),
                'created_at'    => $faker->dateTimeBetween('-1 years', 'now')
            ]);
        }


        // create groups
        $role1 = DB::table('roles')->insert([
            'name'      => 'Administrator',
            'alias'     => 'Admin',
            'type'      => 'admin',
            'hierarchy' => 0
        ]);

        $role2 = DB::table('roles')->insert([
            'name'      => 'Super Operator',
            'alias'     => 'SOP',
            'type'      => 'default',
            'hierarchy' => 1
        ]);

        $role3 = DB::table('roles')->insert([
            'name'      => 'Operator',
            'alias'     => 'OP',
            'type'      => 'default',
            'hierarchy' => 3
        ]);

        $role4 = DB::table('roles')->insert([
            'name'      => 'Moderator',
            'alias'     => 'Mod',
            'type'      => 'default',
            'hierarchy' => 4
        ]);

        $role5 = DB::table('roles')->insert([
            'name'      => 'Trusted',
            'alias'     => '',
            'type'      => 'default',
            'hierarchy' => 5
        ]);

        $role6 = DB::table('roles')->insert([
            'name'      => 'Member',
            'alias'     => '',
            'type'      => 'default',
            'hierarchy' => 6
        ]);

        $role7 = DB::table('roles')->insert([
            'name'      => 'Guest',
            'alias'     => '',
            'type'      => 'guest',
            'hierarchy' => -1
        ]);

        $role8 = DB::table('roles')->insert([
            'name'      => 'Donator',
            'alias'     => '',
            'type'      => 'default',
            'hierarchy' => -1
        ]);

        // permissions
        $perm_key1 = DB::table('permissions')->insert([
            'permission' => 'forum.thread.create'
        ]);

        $perm_key2 = DB::table('permissions')->insert([
            'permission' => 'forum.post.create'
        ]);

        $perm_key3 = DB::table('permissions')->insert([
            'permission' => 'forum.thread.sticky'
        ]);

        // assign permissions to roles
        $permission1 = DB::table('role_permissions')->insert([
            'role_id'       => $role6,
            'permission_id' => $perm_key1
        ]);

        $permission2 = DB::table('role_permissions')->insert([
            'role_id'       => $role6,
            'permission_id' => $perm_key2
        ]);

        $permission3 = DB::table('role_permissions')->insert([
            'role_id'       => $role3,
            'permission_id' => $perm_key3
        ]);

        // assign some users to roles
        DB::table('role_users')->insert([
            'user_id' => $admin_user,
            'role_id' => $role1,
        ]);

        $existing = array();
        for($i = 0; $i < 6; $i++)
        {
            $index = rand(0, 25);
            while(in_array($index, $existing))
            {
                $index = rand(0, 25);
            }

            $role = "role" . rand(1, 6);
            DB::table('role_users')->insert([
                'user_id' => $user_ids[$index],
                'role_id' => $$role,
            ]);
        }
    }
}
