var is_showing_scroller = false;

$(document).ready(function() {
    $('.scroll-top-wrapper').css({'bottom': '60px'});
    $(document).on( 'scroll', function() {
        scrollCheck();
    });

    scrollCheck();
    generateClouds();
});

function scrollCheck()
{
    if ($(window).scrollTop() > 100)
    {
        if(!is_showing_scroller)
        {
            // show scroll button
            $('.scroll-top-wrapper').css({'bottom': '30px'});
            $('.scroll-top-wrapper').addClass('show');
            is_showing_scroller = true;
        }
    }
    else
    {
        if(is_showing_scroller)
        {
            // hide scroll button
            $('.scroll-top-wrapper').css({'bottom': '60px'});
            $('.scroll-top-wrapper').removeClass('show');
            is_showing_scroller = false;
        }
    }
}

function scrollToTop()
{
    var offset = $('body').offset().top;
    $('html, body').animate({scrollTop: offset}, 200, 'linear');
}

function getRandom(min, max, multiple)
{
    var value = Math.floor(Math.random() * max) + min;

    if(multiple != null)
    {
        value = Math.round(value / multiple) * multiple
    }

    return value;
}

function generateClouds()
{
    var minClouds = 25;
    var maxClouds = 50;

    var numOfClouds = getRandom(minClouds, maxClouds);

    for(i=0; i<numOfClouds; i++)
    {
        var width = $('.layout-background').width() / 1.2;

        var x = getRandom(0, width, 5);
        var y = getRandom(-30, 100, 5);
        var z = getRandom(-10, -50, 5);

        $cloud = $("<div class='cloud'></div>").css({
            width:  getRandom(100, 300) + 'px',
            height: getRandom(5, 10) + 'px',
            "transform": "translate3d(" + x + "px, " + y + "px," + z + "px) rotateX(-45deg)"
        });

        scrollCloud($cloud, x, y, z);

        $cloud.appendTo('.layout-background');
    }
}

scrollCloud = function(cloud, x, y, z)
{
    var maxWidth = $('.layout-background').width();
    var maxDuration = 18000 * (maxWidth / 500);
    var animDuration = Math.floor( (1 - (x / maxWidth)) * maxDuration );

    cloud.animate({
        'opacity': 100
    }, {
        step: function (now, fx) {
            var newX = ((now / 100) * maxWidth) + x;
            cloudAnimateStep(cloud, newX, y, z);
        },
        duration: animDuration,
        easing: 'linear',
        complete: function() {
            cloudAnimateStep(cloud, 0, y, z);
            scrollCloud(cloud, 0, y, z);
        }
    }, 'linear');
}

cloudAnimateStep = function(cloud, x, y, z)
{
    cloud.css({"transform": "translate3d(" + x + "px, " + y + "px, " + z + "px) rotateX(-45deg)"});
}