var Vue = require('vue');
var VueRouter = require('vue-router');
var VueResource = require('vue-resource');
var VueValidator = require('vue-validator');

Vue.use( VueRouter );
Vue.use( VueResource );
Vue.use( VueValidator );

import App from './routes/app.vue';

Vue.config.debug = true;

// grab csrf token from page meta data
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');

// set the api root url
Vue.http.options.root = 'http://localhost/PCB_update/public/api';

// push authorisation token into all http requests if available
import AuthService from './services/auth';
AuthService.pushHeader(Vue);

// components
Vue.component('side-donate', require('./components/side-donate.vue'));

// setup router
var router = new VueRouter({
    root: '/',
    //hashbang:false,
    //history:true
});

// all routes are either bundled within app.js (require) or loaded async at runtime (loadjs)
router.map({
    '/': {
        component: require('./routes/home.route.vue')
    },
    '/info': {
        component: require('./routes/info.route.vue'),
        subRoutes: {
            '/faq': {
                component: require('./routes/info.faq.route.vue')
            },
            '/rules': {
                component: require('./routes/info.rules.route.vue')
            },
            '/commands': {
                component: require('./routes/info.commands.route.vue')
            },
            '/ranks': {
                component: require('./routes/info.ranks.route.vue')
            },
            '/economy': {
                component: require('./routes/info.economy.route.vue')
            },
            '/staff': {
                component: require('./routes/info.staff.route.vue')
            }
        }
    },
    '/register': {
        component: require('./routes/register.route.vue'),
        subRoutes: {
            '/': {
                component: require('./routes/register.step1.route.vue')
            },
            '/optional': {
                component: require('./routes/register.step2.route.vue')
            }
        }
    },
    '/community/banappeal': {
        component: require('./routes/banappeal.route.vue')
    },
    '/stats/banlist': {
        component: require('./routes/banlist.route.vue')
    },
    '/stats/maps': {
        component: require('./routes/maps.route.vue')
    },
    '/server/:id/status': {
        component: require('./routes/server.status.route.vue')
    },
    '/donate': {
        component: require('./routes/donate.route.vue')
    },
    '/user/:id': {
        component: require('./routes/user.route.vue')
    },
    '/forums': {
        component: function (resolve) {
            loadjs(['./resources/assets/js/routes/forums.route.vue'], resolve)
        },
        subRoutes: {
            '/': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/forums.index.route.vue'], resolve)
                }
            },
            '/board/:board': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/forums.board.route.vue'], resolve)
                }
            },
            '/topic/:topic': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/forums.thread.route.vue'], resolve)
                }
            },
            '/board/:board/topic': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/forums.new.thread.route.vue'], resolve)
                }
            },
            '/memberlist': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/forums.memberlist.route.vue'], resolve)
                }
            }
        }
    },
    '/staff': {
        component: function (resolve) {
            loadjs(['./resources/assets/js/routes/staff/admin.route.vue'], resolve)
        },
        subRoutes: {
            '/': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/staff/admin.index.route.vue'], resolve)
                }
            },
            '/forum': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/staff/admin.forum.route.vue'], resolve)
                },
                subRoutes: {
                    '/': {
                        component: function (resolve) {
                            loadjs(['./resources/assets/js/routes/staff/admin.forum.boards.route.vue'], resolve)
                        }
                    },
                    '/categories/new': {
                        component: function (resolve) {
                            loadjs(['./resources/assets/js/routes/staff/admin.forum.boards.new.category.route.vue'], resolve)
                        }
                    },
                    '/categories/edit/:id': {
                        component: function (resolve) {
                            loadjs(['./resources/assets/js/routes/staff/admin.forum.boards.new.category.route.vue'], resolve)
                        }
                    }
                }
            },
            '/servers': {
                component: function (resolve) {
                    loadjs(['./resources/assets/js/routes/staff/admin.servers.route.vue'], resolve)
                },
                subRoutes: {
                    '/': {
                        component: function (resolve) {
                            loadjs(['./resources/assets/js/routes/staff/admin.servers.list.route.vue'], resolve)
                        }
                    }
                }
            }
        }
    }
});

router.beforeEach(function () {
    //window.scrollTo(0, 0);
});

router.redirect({
    '*': '/'
});

router.start(App, '#app');