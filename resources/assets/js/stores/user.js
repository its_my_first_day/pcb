export default {

    state: {
        authorised: false,
        user: {
            id: -1,
            username: '',
            alias: '',
            displayName: '',
            avatar: ''
        }
    },

    setUser: function(user) {
        this.state.user = user;
        this.state.authorised = (user !== null && user !== 'undefined');

        // add 'computed' values
        this.state.user['displayName'] = (this.state.user.alias === null) ? this.state.user.username : this.state.user.alias;
        this.state.user['avatar'] = 'https://minotar.net/avatar/' + this.state.user.displayName + '/16.png';
    },

    clearUser: function() {
        this.state.authorised = false;
        this.state.user = {
            id: -1,
            username: '',
            alias: '',
            displayName: '',
            avatar: ''
        }
    }
}