<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumThread extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_threads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['board_id', 'root_id', 'root_type', 'title', 'is_locked', 'is_sealed', 'is_stickied', 'is_archived', 'last_action_at', 'last_post_at', 'view_count', 'reply_count', 'last_post_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public function Root()
    {
        return $this->morphTo('root');
    }

    public function Actions()
    {
        return $this->hasMany('App\Http\Models\ForumThreadAction', 'thread_id');
    }

    public function Poll()
    {
        return $this->hasOne('App\Http\Models\ForumPoll', 'thread_id');
    }

    public function Board()
    {
        return $this->belongsTo('App\Http\Models\ForumBoard', 'board_id');
    }

    public function Reads()
    {
        return $this->hasMany('App\Http\Models\ForumRead', 'thread_id');
    }

    public function LatestPost()
    {
        return $this->hasOne('App\Http\Models\ForumPost', 'id', 'last_post_id');
    }
}
