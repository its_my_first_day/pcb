<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class EconomyTransaction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'economy_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_user_id',
        'shop_uuid',
        'customer_user_id',
        'customer_uuid',
        'item',
        'transaction_type',
        'amount',
        'quantity',
        'time'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public $timestamps = false;
}
