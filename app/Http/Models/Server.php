<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Server extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'ip', 'port', 'type', 'tag', 'show_port'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function LatestStatus()
    {
        return $this->hasOne('App\Http\Models\ServerStatus', 'server_id', 'id')->orderBy('date', 'desc');
    }

    public function Statuses()
    {
        return $this->hasMany('App\Http\Models\ServerStatus', 'server_id', 'id')->orderBy('date', 'asc');
    }
}
