<?php

namespace App\Http\Models;

use App\Http\Traits\ThreadModel;
use Illuminate\Database\Eloquent\Model;

class ForumAnnouncement extends Model
{
    use ThreadModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_announcements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'message', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
