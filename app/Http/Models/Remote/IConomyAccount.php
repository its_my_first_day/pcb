<?php

namespace App\Http\Models\Remote;

use Illuminate\Database\Eloquent\Model;

class IConomyAccount extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mc_iConomy';

    protected $connection = 'live_server';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public $timestamps = false;
}
