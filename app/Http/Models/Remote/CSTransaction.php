<?php

namespace App\Http\Models\Remote;

use Illuminate\Database\Eloquent\Model;

class CSTransaction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'csnUUID';

    protected $connection = 'pcbmc.co';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;
}
