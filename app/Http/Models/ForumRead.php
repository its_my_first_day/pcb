<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumRead extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_thread_read';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'thread_id', 'read_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public $timestamps = false;
}
