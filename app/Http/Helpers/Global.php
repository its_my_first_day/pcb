<?php
/**
 * Outputs an entire array (formatted)
 * @param $array
 */
function PrintArray($array)
{
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function GetAvatar($username, $size = 16)
{
    echo '<img src="https://minotar.net/avatar/' . $username . '/' . $size . '.png" />';
}