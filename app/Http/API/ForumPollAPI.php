<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\ForumPoll;
use App\Http\Models\ForumPollChoice;
use App\Http\Models\ForumPollVote;
use App\Http\Models\ForumPost;
use App\Http\Models\ForumThread;
use App\Http\Models\ForumThreadAction;
use App\Http\Models\ForumAnnouncement;
use App\Http\Models\User;

use Carbon\Carbon;
use DB;
use Dingo\Api\Exception\ResourceException;
use JWTAuth;
use Input;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;
use HTMLPurifier;
use HTMLPurifier_Config;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ForumPollAPI extends BaseAPI {

    public function Create()
    {
        $thread_id          = Input::get('thread_id');
        $question           = Input::get('question');
        $choices            = json_decode( Input::get('choices') );
        $max_choices        = Input::get('max_choices');
        $vote_req_post      = Input::get('vote_req_post');
        $anonymous_votes    = Input::get('anonymous_votes');
        $votes_visible      = Input::get('votes_visible');

        $poll = ForumPoll::create([
            'thread_id'             => $thread_id,
            'question'              => $question,
            'max_choices'           => $max_choices,
            'vote_requires_post'    => $vote_req_post,
            'anonymous_votes'       => $anonymous_votes,
            'votes_visible'         => $votes_visible,
        ]);

        $all_choices = array();
        foreach($choices as $choice)
        {
            array_push($all_choices, [
                'poll_id'   => $poll->id,
                'choice'    => $choice
            ]);
        }
        ForumPollChoice::insert($all_choices);

        return null;
    }

    public function Vote($id)
    {
        //TODO: do auth check in a service instead
        $user = JWTAuth::parseToken()->authenticate();

        $selection = Input::get('selection');

        // ensure user hasn't already voted on this poll
        $existing_vote = ForumPollVote::where('user_id', $user->id)
            ->where('poll_id', $id)
            ->first();

        if($existing_vote !== null)
            throw new BadRequestHttpException('User has already voted.');

        // ensure poll exists
        $poll = ForumPoll::find($id);
        if($poll == null)
            throw new BadRequestHttpException('Poll does not exist.');

        // otherwise create vote
        $vote = ForumPollVote::create([
            'user_id'       => $user->id,
            'poll_id'       => $id,
            'choice_id'     => $selection,
            'created_at'    => Carbon::now()
        ]);

        return [
            'success'   => true
        ];
    }
}