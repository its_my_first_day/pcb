<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\Setting;
use App\Http\Models\ForumThread;

use DB;
use JWTAuth;
use Input;
use Validator;

use Dingo\Api\Exception\ResourceException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ForumCategoryAPI extends BaseAPI {

    public function Create()
    {
        $name = Input::get('name');
        $position = Input::get('position');

        // validate input
        $validator = Validator::make([
            'name' => $name,
            'position' => $position
        ], [
            'name'  => 'required|min:1|max:50',
            'position'  => 'required|numeric',
        ]);
        if($validator->fails())
            throw new ResourceException('Invalid form input.', $validator->errors());

        // reshuffle the orders by slotting in the new category
        $categories = ForumCategory::orderBy('order', 'asc')->get();
        $takenSlot = $position;
        foreach($categories as $cat)
        {
            if($cat->order == $takenSlot)
            {
                if($cat->id == $id) break;

                $cat->order += 1;
                $cat->save();
                $takenSlot += 1;
            }
            elseif($cat->order > $takenSlot)
            {
                break;
            }
        }

        $category = ForumCategory::create([
            'name'   => $name,
            'order' => $position
        ]);

        return $category;
    }

    public function Save($id)
    {
        $name = Input::get('name');
        $position = Input::get('position');

        // validate input
        $validator = Validator::make([
            'name' => $name,
            'position' => $position
        ], [
            'name'  => 'required|min:1|max:50',
            'position'  => 'required|numeric',
        ]);
        if($validator->fails())
            throw new ResourceException('Invalid form input.', $validator->errors());

        // reshuffle the orders by slotting in the edited category
        $categories = ForumCategory::orderBy('order', 'asc')->get();
        foreach($categories as $cat)
        {
            if($cat->id == $id)
            {
                $cat->name = $name;
                $cat->order = $position;
            }
            else
            {
                if($cat->order >= $position)
                {
                    $cat->order += 1;
                }
            }
        }

        // loop through again and maintain a consecutive ordering with no gaps starting from 1
        $categories = $categories->sortBy('order');

        $slot = 1;
        foreach($categories as $cat)
        {
            $cat->order = $slot;
            $cat->save();
            $slot++;
        }

        return [
            'success'    => true
        ];
    }

    public function Get($id)
    {
        $category = ForumCategory::find($id);
        if($category == null)
            throw new NotFoundHttpException('Category not found.');

        return $category;
    }

    public function All()
    {
        return ForumCategory::orderBy('order', 'asc')
            ->get();
    }

    public function Reorder($id)
    {
        $category = ForumCategory::find($id);
        if($category == null)
            throw new NotFoundHttpException('Category not found.');

        $direction = Input::get('direction');
        if($direction == null)
            throw new ResourceException('Input missing.');

        if($direction !== 1 && $direction !== -1)
            throw new BadRequestHttpException('Invalid Direction input.');

        $new_order = $category->order + $direction;
        $categories = ForumCategory::orderBy('order', 'asc')->get();
        if($new_order <= 0)
            throw new BadRequestHttpException('Already at the highest position');

        if($new_order > count($categories))
            throw new BadRequestHttpException('Already at the lowest position');

        foreach($categories as $cat)
        {
            if($cat->order == $new_order)
            {
                $cat->order = $category->order;
                $cat->save();
                break;
            }
        }

        $category->order = $new_order;
        $category->save();

        $app = app();
        $controller = $app->make('\App\Http\Controllers\ForumBoardController');
        return $controller->callAction('GetAll', $parameters = array());
    }

    public function Delete($id)
    {
        $category = ForumCategory::find($id);
        if($category == null)
            throw new NotFoundHttpException('Category not found.');

        $category->delete();

        // update category orders
        $slot = 1;
        $categories = ForumCategory::orderBy('order', 'asc')->get();
        foreach($categories as $cat)
        {
            if($cat->order == $slot)
                continue;

            $cat->order = $slot;
            $cat->save();
            $slot++;
        }

        return [
            'success' => true
        ];
    }

}