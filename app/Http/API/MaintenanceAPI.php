<?php
namespace App\Http\API;

use App\Http\Models\ForumBoard;
use App\Http\Models\ForumPost;
use App\Http\Models\ForumRead;
use App\Http\Models\ForumThread;
use App\Http\Models\Setting;
use Carbon\Carbon;

class MaintenanceAPI extends BaseAPI {

    public function RecountBoardStats()
    {
        $boards = ForumBoard::get();
        foreach($boards as $board)
        {
            $threads = ForumThread::where('board_id', $board->id)
                ->with('Root')
                ->get();

            $total_board_posts = 0;
            foreach($threads as $thread)
            {
                $total_posts = 0;
                $posts = $thread->root->Posts;
                $total_posts += count($posts);
                foreach($posts as $post)
                {
                    $replies = $post->replies;
                    $total_posts += count($replies);
                }

                $thread->reply_count = $total_posts;
                $thread->save();

                $total_board_posts += $total_posts;
            }

            $board->total_threads = count($threads);
            $board->total_posts = $total_board_posts;
            $board->save();
        }
    }

    /**
     * Clears any 'thread read' entries older than X days
     */
    public function ClearRead()
    {
        $max_days = Setting::where('key', 'web.read_storage_threshold')->first()->value;
        $threshold_date = Carbon::now()->subDays($max_days);

        ForumRead::where('read_at', '<=', $threshold_date)->delete();
    }

}