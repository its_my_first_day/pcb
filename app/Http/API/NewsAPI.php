<?php
namespace App\Http\API;

use App\Http\Models\ForumAnnouncement;
use App\Http\Models\Setting;
use DB;
use JWTAuth;
use Input;
use Validator;

class NewsAPI extends BaseAPI {

    public function GetLatest()
    {
       $board = Setting::where('key', 'web.announcement_board')->first();
        if(!$board)
            throw new Exception('Announcement board not set');

        $threads = ForumAnnouncement::whereHas('Thread', function($q) use($board) {
                $q->where('board_id', '=', $board->value);
            })
            ->with('Thread')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();

        return $threads;
    }

}