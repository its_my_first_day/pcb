<?php
namespace App\Http\API;

use App\Http\Models\User;
use JWTAuth;
use Input;
use Validator;

class RegisterAPI extends BaseAPI {

    /**
     * POST: Creates a new User account after verifying input
     *
     * @return array
     */
    public function CreateAccount()
    {
        $rules = [
            'username'  => 'required|max:50|unique:users',
            'password'  => 'required|max:60|min:4',
            'password2' => 'same:password',
            'email'     => 'required|max:50|email|unique:users',
            'recaptcha'  => 'required'
        ];

        $messages = [
            'required'              => 'The :attribute field cannot be blank.',
            'password2.same'        => 'The password fields do not match.',
            'password2.required'    => 'The password confirmation field cannot be blank.',
            'recaptcha.required'    => 'Please click the reCAPTCHA checkbox'
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        if($validator->fails())
        {
            return [
                'success'   => false,
                'errors'    => $validator->errors()
            ];
        }

        // create the user
        $user = User::create([
            'username'  => Input::get('username'),
            'alias'     => Input::get('alias'),
            'password'  => Hash::make( Input::get('password') ),
            'email'     => Input::get('email')
        ]);

        $token = JWTAuth::fromUser($user);

        // TODO: link player stats?
        // TODO: verify recaptcha

        return [
            'success'   => true,
            'token'     => $token
        ];
    }

    /**
     * POST: Checks whether a username is available or taken
     *
     * @return array
     */
    public function CheckUsername()
    {
        $username = Input::get('username');
        $user = User::where('username', $username)->first();
        $is_taken = $user != null;

        return [
            'success'   => true,
            'taken'     => $is_taken
        ];
    }

}