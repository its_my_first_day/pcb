<?php
namespace App\Http\API;

use Input;
use Auth;
use JWTAuth;

class AuthAPI extends BaseAPI {

    /**
     * POST: Authenticates a username/email and password combination.
     * Generates user token on success.
     *
     * @return array
     */
    public function Login()
    {
        $username = Input::get('username');
        $is_email = filter_var($username, FILTER_VALIDATE_EMAIL);

        $input = ['password' => Input::get('password')];
        if($is_email)
            $input['email'] = $username;
        else
            $input['username'] = $username;

        if(Auth::once($input))
        {
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);

            return [
                'success'   => true,
                'token'     => $token,
                'user'      => [
                    'id'        => $user->id,
                    'username'  => $user->username,
                    'alias'     => $user->alias
                ]
            ];
        }

        return [
            'success' => false
        ];
    }

    /**
     * GET: Converts a user token (from header) back into a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function TokenToUser()
    {
        $user = $this->GetUser();
        if($user) {
            return $user;
        }

        // no user retrieved - attempt to refresh token
        $token = JWTAuth::getToken();
        if(!$token)
            throw new BadRequestHtttpException('Token not provided');

        try
        {
            $token = JWTAuth::refresh($token);
        }
        catch(TokenInvalidException $e)
        {
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return $this->response->withArray(['token'=>$token]);
    }

}