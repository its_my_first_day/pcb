<?php
namespace App\Http\Processors\Servers;

use TeamSpeak3\TeamSpeak3;
use Log;

class QueryTeamSpeak implements IServerQuery
{
    public function GetStatus($ip, $port)
    {
        $status = array();

        $user = "";
        $pass = "";
        $query_port = 10011;

        try
        {
            $ts3 = TeamSpeak3::factory("serverquery://".$user.":".$pass."@".$ip.":".$query_port."/?server_port=".$port."&use_offline_as_virtual=1&no_query_clients=1");

            $status['online'] = $ts3->getProperty("virtualserver_status");
            $status['players'] = $ts3->getProperty("virtualserver_clientsonline") - $ts3->getProperty("virtualserver_queryclientsonline");
            $status['max_players'] = $ts3->getProperty("virtualserver_maxclients");
        }
        catch (Exception $e)
        {
            $status['online'] = false;
            Log::notice('TeamSpeak query exception (' . $e->getCode() . '): ' . $e->getMessage());
        }

        return $status;
    }
}