<?php
namespace App\Http\Processors\Servers;

interface IServerQuery
{
    function GetStatus($ip, $port);
}