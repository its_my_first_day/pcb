<?php
namespace App\Http\Processors\RemoteSync;

/**
 * Connects to a remote database, downloads data and mutates it into the specified format
 *
 * Class RemoteSync
 * @package App\Http\Processors\RemoteSync
 */

use Cache;

class RemoteSync {

    private $cache_key = 'remote-sync';

    private $connection;
    private $syncer;

    /**
     * @param string $connection            The connection (specified in config) to connect to
     * @param AbstractRemoteSyncer $syncer  The syncer to handle the job
     */
    public function __construct($connection, $syncer)
    {
        $this->connection = $connection;
        $this->syncer = $syncer;
    }

    /**
     * Downloads data from the remote server to the local server (incrementally by 'last id')
     * @param int $take     Number of rows to retrieve. -1 = infinite
     */
    public function PullIncrement($take = -1)
    {
        // get cached 'last id'
        $cache = Cache::get($this->cache_key);
        if($cache == null)
            $cache = array();

        if(!array_key_exists($this->syncer->key, $cache))
            $cache[$this->syncer->key] = -1;

        $last_id = $cache[$this->syncer->key];

        // connect to the remote server db
        $live_model = new $this->syncer->remote_model();
        $live_model->setConnection($this->connection);
        $rows = $live_model->orderBy($this->syncer->id_column, 'asc')
            ->where('id', '>', $last_id);

        if($take > 0)
            $rows = $rows->take($take);

        $rows = $rows->get();

        // parse every row
        $i = 0;
        $row_count = count($rows);
        $rows_to_import = array();
        $id_col = $this->syncer->id_column;
        foreach($rows as $row)
        {
            $new_row = $this->syncer->OnParseRow($row);
            array_push($rows_to_import, $new_row);
            $last_id = $row->$id_col;

            $i++;
            // if limit reached, fire event OnComplete()
            if($this->syncer->process_limit == -1)
                continue;

            if($i != $row_count && $this->syncer->process_limit % $i == 0)
            {
                $this->syncer->OnBulkImport($rows_to_import);
                $rows_to_import = array();
            }
        }
        $this->syncer->OnBulkImport($rows_to_import);

        // recache the 'last id'
        $cache[$this->syncer->key] = $last_id;
        Cache::forever($this->cache_key, $cache);
    }

    public function PullEverything()
    {
        $live_model = new $this->syncer->remote_model();
        $live_model->setConnection($this->connection);
        $rows = $live_model->orderBy($this->syncer->id_column, 'asc')
            ->get();

        // parse every row
        $i = 0;
        $row_count = count($rows);
        $rows_to_import = array();
        foreach($rows as $row)
        {
            $new_row = $this->syncer->OnParseRow($row);
            array_push($rows_to_import, $new_row);

            $i++;
            // if limit reached, fire event OnComplete()
            if($this->syncer->process_limit == -1)
                continue;

            if($i != $row_count && $this->syncer->process_limit % $i == 0)
            {
                $this->syncer->OnBulkImport($rows_to_import);
                $rows_to_import = array();
            }
        }
        $this->syncer->OnBulkImport($rows_to_import);
    }

}