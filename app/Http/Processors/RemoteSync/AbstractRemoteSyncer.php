<?php
namespace App\Http\Processors\RemoteSync;

abstract class AbstractRemoteSyncer {

    public $key;

    public $remote_model;

    public $local_model;

    /**
     * The column in the remote model that is auto-incremented
     * @var
     */
    public $id_column = "id";

    /**
     * Number of rows to process before running OnComplete() and then looping.
     * -1 is no limit
     * @var int
     */
    public $process_limit = -1;


    /**
     * Logic to apply to every row in the remote table.
     * Mutation logic should go here.
     *
     * @param $row
     * @return array    A single associative array to be imported locally as a row
     */
    public abstract function OnParseRow($row);

    /**
     * Invoked when either all rows are processed or the limit is reached.
     * Local db import logic should go here.
     *
     * @param array $rows   An entire array of associative arrays. Each row corresponds to a single row to import
     */
    public abstract function OnBulkImport($rows);
}