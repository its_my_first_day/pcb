<?php
namespace App\Http\Processors\Stats;

use App\Http\Models\ServerStatus;
use Carbon\Carbon;

class ServerStatsPlayerCount {

    private $graph_limit = 25;
    private $overall_avg = array();

    public function Get($server_id, $date_range, $min_interval = 5)
    {
        return $this->Build($server_id, $date_range, $min_interval);
    }

    /**
     * Gets the average 'players online' count between two dates
     *
     * @param $server_id            Server ID to query
     * @param array $date_range     Date range limit
     * @param int $min_interval     Averages the player counts into groups of X (interval 5 = no averaging)
     *
     * @return array
     */
    private function Build($server_id, $date_range, $min_interval)
    {
       $statuses = ServerStatus::where('server_id', $server_id);

       if($date_range != null)
           $statuses = $statuses->whereBetween('date', $date_range);

       $statuses = $statuses->orderBy('date', 'desc')
           ->get()
           ->reverse();

        // average the statuses into the given min interval
        $avg_statuses = array();
        $labels = array();
        $height = 15;
        if($min_interval > 0)
        {
            $upper_date = new Carbon($statuses[0]->date);
            $upper_date->addMinutes($min_interval);

            $current_group = array();
            foreach($statuses as $status)
            {
                $date = new Carbon($status->date);
                if($date->gt($upper_date))
                {
                    $average = 0;
                    for($i = 0; $i < count($current_group); $i++)
                    {
                        $average += $current_group[$i];
                    }
                    $average = round($average / count($current_group));

                    $newHeight = min(80, (floor($average / 5) * 5) + 5);
                    if($newHeight > $height)
                        $height = $newHeight;

                    array_push($avg_statuses, $average);
                    array_push($labels, $upper_date->format('h'));
                    $current_group = array();

                    $upper_date->addMinutes($min_interval);
                }

                array_push($current_group, $status->current_players);
            }
            if($current_group != null)
            {
                $average = 0;
                for($i = 0; $i < count($current_group); $i++)
                {
                    $average += $current_group[$i];
                }
                $average = round($average / count($current_group));
                array_push($avg_statuses, $average);
            }

            if(count($avg_statuses) > $this->graph_limit)
            {
                $offset = count($avg_statuses) - $this->graph_limit;
                $avg_statuses   = array_slice($avg_statuses, $offset, $this->graph_limit);
                $labels         = array_slice($labels, $offset, $this->graph_limit);
            }
        }

        return [
            'series' => $avg_statuses,
            'labels' => $labels,
            'height' => $height
        ];
    }

    /**
     * Gets the all-time average 'players online' count for the given period
     *
     * @param $server_id            Server ID to query
     * @param array $date_range     Date range limit
     * @param int $min_interval     Averages the player counts into groups of X (interval 5 = no averaging)
     *
     * @return array
     */
    private function Average($server_id, $min_interval)
    {
        // TODO: needs to organise statuses into time of day

        $this->overall_avg = array();
        $statuses = ServerStatus::where('server_id', $server_id)
            ->orderBy('date', 'desc')
            ->chunk(500, function($chunk) use($min_interval) {

                $upper_date = new Carbon($chunk[0]->date);
                $upper_date->subMinutes($min_interval);
                $avg_statuses = array();
                $current_group = array();

                // place statuses into interval groups
                foreach($chunk as $status)
                {
                    $date = new Carbon($status->date);
                    if($date->lt($upper_date))
                    {
                        $average = 0;
                        for($i = 0; $i < count($current_group); $i++)
                        {
                            $average += $current_group[$i];
                        }
                        $average = round($average / count($current_group));

                        array_push($avg_statuses, $average);
                        $current_group = array();

                        $upper_date->subMinutes($min_interval);
                    }

                    array_push($current_group, $status->current_players);
                }

                // place any remaining statuses into one final group
                if($current_group != null)
                {
                    $average = 0;
                    for($i = 0; $i < count($current_group); $i++)
                    {
                        $average += $current_group[$i];
                    }
                    $average = round($average / count($current_group));
                    array_push($avg_statuses, $average);
                }

                // combine the groups with the 'overall' average
                $this->overall_avg = array_merge($avg_statuses, $this->overall_avg);
            });

        return $this->overall_avg;
    }
}