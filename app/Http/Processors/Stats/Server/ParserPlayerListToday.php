<?php
namespace App\Http\Processors\Stats\Server;

use Carbon\Carbon;
use Cache;

class ParserPlayerListToday extends AbstractServerStatParser {

    protected $cache_key = 'server_stats-parser-player_list_today';
    protected $output_name = 'players_today';

    private $player_data;
    private $lowest_date;
    private $last_status;

    /**
     * Determines who has been online today.
     * Calculates:
     *  - Playtime today
     *  - Playtime this session (if online)
     *
     * @param mixed $status
     * @return mixed|void
     */
    public function Parse($status)
    {
        $server_id = $status->server_id;
        if($this->player_data == null)
        {
            $is_entry = $this->GetCacheEntry($server_id, $this->player_data, function() use($status) {
                return [
                    'today'     => array(),
                    'online'    => array()
                ];
            });

            $this->last_status = $status;
            $this->lowest_date = Carbon::now()->subDay(1);

            if(!$is_entry)
                return;
        }

        $status_date = new Carbon($status->date);

        if($status_date->gte($this->lowest_date))
        {
            $last_status_date = new Carbon($this->last_status->date);
            $time_diff = $status_date->diffInMinutes($last_status_date);

            $players = explode(",", $status->players);
            foreach($players as $player)
            {
                if($player == "") continue;

                if(array_key_exists($player, $this->player_data['today']))
                    $this->player_data['today'][$player] += $time_diff;
                else
                    $this->player_data['today'][$player] = 0;

                if(array_key_exists($player, $this->player_data['online']))
                    $this->player_data['online'][$player] += $time_diff;
                else
                    $this->player_data['online'][$player] = 0;
            }

            // maintain online list by checking for differences
            $last_status_online = array_keys($this->player_data['online']);
            $still_online = array_intersect($players, $last_status_online);
            $now_offline = array_diff($last_status_online, $still_online);

            // remove players who are no longer online
            foreach($now_offline as $offline_player)
            {
                unset($this->player_data['online'][$offline_player]);
            }
        }

        $this->last_status = $status;
    }


    /**
     * Persists the streak data in storage
     *
     * @return mixed|void
     */
    public function OnComplete()
    {
        if($this->last_status != null)
        {
            $cache = Cache::get($this->cache_key);
            $cache[$this->last_status->server_id] = $this->player_data;
            Cache::forever($this->cache_key, $cache);
        }
    }

    public function OnFirstRun()
    {
        Cache::forget($this->cache_key);
    }
}