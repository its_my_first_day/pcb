<?php
namespace App\Http\Processors\Stats\Server;

use App\Http\Models\PlayerStats;
use Cache;
use Carbon\Carbon;

class ParserWeeklyOnline extends AbstractServerStatParser {

    protected $cache_key = 'server_stats-parser-weekly-online';
    protected $output_name = 'players_online_weekly';

    private $cache;
    private $last_server_id;
    private $all_time_data;
    private $this_week_data;
    private $this_week;

    private $current_day;
    private $current_stats;

    /**
     * Calculate the weekly graph data:
     *  - Average simultaneous players p/dow
     *  - Max simultaneous players p/dow
     *  - Total players p/dow
     *
     * @param $status
     * @return mixed|void
     */
    public function Parse($status)
    {
        $server_id = $status->server_id;
        $this->last_server_id = $server_id;

        // retrieve cache data if it exists - otherwise build it and then exit
        if($this->cache == null)
        {
            $this->GetCacheEntry($server_id, $this->cache, function() {
                $default = array();
                for($i = 0; $i<7; $i++)
                {
                    $default[$i] = [
                        'average'   => ['count' => 0, 'total' => 0],
                        'max'       => ['count' => 0, 'total' => 0],
                        'total'     => 0
                    ];
                }

                return [
                    'all'       => $default,
                    'this_week' => $default
                ];
            });

            $this->this_week = Carbon::now()->subWeek(1);

            $this->all_time_data  = $this->cache['all'];
            $this->this_week_data = $this->cache['this_week'];
        }


        // process a status by day
        $date = new Carbon($status->date);
        $dow = $date->dayOfWeek;

        if($this->current_day == null || $date->day != $this->current_day->day)
        {
            // new day - push a summary of the last day to memory
            if($this->current_day != null)
            {
                $this->all_time_data[$dow]['average']['count'] += 1;
                $this->all_time_data[$dow]['average']['total'] += $this->current_stats['average']['total'] / $this->current_stats['average']['count'];
                $this->all_time_data[$dow]['max']['count'] += 1;
                $this->all_time_data[$dow]['max']['total'] += $this->current_stats['max'];

                // if status occured this week, compile additional stats
                if($date->gte($this->this_week))
                {
                    $this->this_week_data[$dow]['average']['count'] += $this->current_stats['average']['count'];
                    $this->this_week_data[$dow]['average']['total'] += $this->current_stats['average']['total'];
                    $this->this_week_data[$dow]['max']['count'] = 1;
                    $this->this_week_data[$dow]['max']['total'] += $this->current_stats['max'];
                }
            }

            $this->current_day = $date;
            $this->current_stats = [
                'average'   => ['count' => 0, 'total' => 0],
                'max'       => 0,
                'total'     => 0
            ];
        }

        $this->current_stats['average']['count'] += 1;
        $this->current_stats['average']['total'] += $status->current_players;

        if($status->current_players > $this->current_stats['max'])
            $this->current_stats['max'] = $status->current_players;

    }


    public function OnComplete()
    {
        if($this->current_stats != null)
        {
            $this->cache[$this->last_server_id] = [
                'all'       => $this->all_time_data,
                'this_week' => $this->this_week_data
            ];

            Cache::forever($this->cache_key, $this->cache);
        }
    }

    public function OnFirstRun()
    {
        Cache::forget($this->cache_key);
    }
}