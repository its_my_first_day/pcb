<?php
namespace App\Http\Processors\Stats\Server;

use Carbon\Carbon;
use Cache;

class ParserUptime extends AbstractServerStatParser {

    protected $cache_key = 'server_stats-parser-uptime';
    protected $output_name = 'server_uptime';

    private $current_streak;
    private $longest_streak = array();
    private $average_streak = array();
    private $server_id;

    /**
     * Calculates:
     *  - the current uptime/downtime of the server
     *  - the longest uptime & downtime of the server
     *
     * @param $status
     * @return mixed|void
     */
    public function Parse($status)
    {
        $server_id = $status->server_id;
        $this->server_id = $server_id;

        if($this->current_streak == null)
        {
            $cache = array();
            $is_entry = $this->GetCacheEntry($server_id, $cache, function() use($status) {
                return [
                    'current'   => new UptimeStreak($status->date, $status->is_online),
                    'average'   => [
                        'online'    => ['count' => 0, 'total' => 0],
                        'offline'   => ['count' => 0, 'total' => 0]
                    ],
                    'longest'   => ['online' => null, 'offline' => null]
                ];
            });

            $this->current_streak = $cache['current'];
            $this->average_streak = $cache['average'];
            $this->longest_streak = $cache['longest'];

            if(!$is_entry)
                return;
        }

        // check if the streak continues
        if($status->is_online == $this->current_streak->is_online)
        {
            $status_date = new Carbon($status->date);
            $streak_date = new Carbon($this->current_streak->streak_end);

            if($streak_date == null || $status_date->gt($streak_date))
                $this->current_streak->streak_end = $status->date;
        }
        else
        {
            // check if this is a new record length
            $online = $this->GetStatus($this->current_streak->is_online);
            if($this->longest_streak[$online] == null
                || $this->current_streak->GetLength() >= $this->longest_streak[$online]->GetLength())
            {
                $this->longest_streak[$online] = $this->current_streak;
            }

            // add to average
            $this->average_streak[$online]['count'] += 1;
            $this->average_streak[$online]['total'] += $this->current_streak->GetLength();

            // update with new streak
            $this->current_streak = new UptimeStreak($status->date, $status->is_online);
        }

    }

    /**
     * Converts is_online boolean to 'online'/'offline' string
     *
     * @param $is_online
     * @return string
     */
    private function GetStatus($is_online)
    {
        if($is_online)
            return 'online';

        return 'offline';
    }

    /**
     * Persists the streak data in storage
     *
     * @return mixed|void
     */
    public function OnComplete()
    {
        $cache = Cache::get($this->cache_key);
        $cache[$this->server_id]['current'] = $this->current_streak;
        $cache[$this->server_id]['average'] = $this->average_streak;
        $cache[$this->server_id]['longest'] = $this->longest_streak;
        Cache::forever($this->cache_key, $cache);
    }

    public function OnFirstRun()
    {
        Cache::forget($this->cache_key);
    }
}

class UptimeStreak
{
    public $streak_start;
    public $streak_end;
    public $is_online;

    /**
     * @param Carbon $start_date
     * @param Bool $is_online   Is the server currently online
     */
    public function __construct($start_date, $is_online)
    {
        $this->streak_start = $start_date;
        $this->streak_end   = $start_date;
        $this->is_online    = $is_online;
    }

    public function GetLength()
    {
        $start  = new Carbon($this->streak_start);
        $end    = new Carbon($this->streak_end);
        return $end->diffInMinutes($start);
    }
}