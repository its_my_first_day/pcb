<?php
namespace App\Http\Processors\Stats\Server;

use Cache;

abstract class AbstractServerStatParser
{
    /**
     * The key to store all server entries under for this operation
     * @var
     */
    protected $cache_key;

    /**
     * The name to use as an array key when getting this stat's data via API
     * @var
     */
    protected $output_name;

    /**
     * Whether this parser will output stats
     * @var bool
     */
    protected $output = true;

    /**
     * The main logic of the operation goes here
     *
     * @param $status   The current server status to be analysed
     * @return mixed
     */
    abstract public function Parse($status);

    /**
     * Called after the entire operation is complete (ie. after every status is parsed for a server).
     * Any clean up or persistence logic should go here
     *
     * @return mixed
     */
    public function OnComplete() {}

    /**
     * Called before Parse() ONCE, when doing a full rebuild of stats from scratch.
     * (ie. when parsing every status available)
     *
     * @return mixed
     */
    public function OnFirstRun() {}

    /**
     * Retrieves data from cache for the specified $server_id.
     * Stores a default value in cache if no entry found
     *
     * @param int $server_id        The id of the server
     * @param array $storage_var    Reference variable to store the found/new cache data in
     * @param callable $default     (Closure) The default data to store in cache if no server entry found
     *
     * @return Was an entry for the given $server_id found?
     */
    protected function GetCacheEntry($server_id, &$storage_var, $default)
    {
        // check for no cache entry at all
        $no_entry = false;
        $cache = Cache::get($this->cache_key);
        if($cache == null)
            $cache = array();

        // check for a cache entry missing this server
        if(!array_key_exists($server_id, $cache))
        {
            // recache the entire array of playtime stats for every server,
            // but only hold onto this server's in memory
            $cache[$server_id] = $default();
            Cache::forever($this->cache_key, $cache);
            $no_entry = true;
        }

        $storage_var = $cache[$server_id];

        return $no_entry;
    }

    public function GetStats()
    {
        if($this->output)
            return [$this->output_name, Cache::get($this->cache_key)];

        return false;
    }
}