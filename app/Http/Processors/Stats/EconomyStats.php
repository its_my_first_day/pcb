<?php
namespace App\Http\Processors\Stats;

use App\Http\Processors\RemoteSync\RemoteSync;
use App\Http\Processors\RemoteSync\RemoteSyncerEconomy;

class EconomyStats {

    /**
     * Import any new transaction data from the server
     * TODO: move this to it's own job process
     */
    public function PullDifferences()
    {
        $syncer = new RemoteSync('pcbmc.co', new RemoteSyncerEconomy());
        $syncer->PullIncrement(10000);
    }

}