<?php

namespace App\Http\Traits;

trait ThreadModel {

    /*public function User()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'user_id');
    }*/

    public function Posts()
    {
        return $this->morphMany('App\Http\Models\ForumPost', 'parent')->with('Replies');
    }

    public function Replies()
    {
        return $this->hasMany('App\Http\Models\ForumPost', 'parent_id');
    }

    public function Likes()
    {
        return $this->hasMany('App\Http\Models\ForumLike', 'parent');
    }


    public function Thread()
    {
        return $this->hasOne('App\Http\Models\ForumThread', 'root_id');
    }



    public function User()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }

    public function Parent()
    {
        return $this->belongsTo('App\Http\Models\ForumPost', 'parent_id');
    }

    public function Parents()
    {
        return $this->belongsTo('App\Http\Models\ForumPost', 'parent_id')->with('Parent');
    }

}