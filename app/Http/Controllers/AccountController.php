<?php

namespace App\Http\Controllers;

use App\Http\API\AuthAPI;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Response;

class AccountController extends Controller
{
    use ThrottlesLogins;

    private $authAPI;
    public function __construct(AuthAPI $authAPI)
    {
        $this->authAPI = $authAPI;
    }

    public function Login()
    {
        return Response::json( $this->authAPI->Login() );
    }

    public function TokenToUser()
    {
        return Response::json( $this->authAPI->TokenToUser() );
    }
}
