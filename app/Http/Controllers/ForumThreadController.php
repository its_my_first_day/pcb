<?php

namespace App\Http\Controllers;

use App\Http\API\ForumThreadAPI;
use Response;

class ForumThreadController extends Controller
{
    private $forumAPI;
    public function __construct(ForumThreadAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function Get($id)
    {
        return $this->forumAPI->GetThread($id);
    }

    public function GetLatest()
    {
        return $this->forumAPI->GetLatest();
    }

    public function Create()
    {
        return $this->forumAPI->CreateThread();
    }

    public function Delete($id)
    {
        return $this->forumAPI->DeleteThread($id);
    }

    public function Lock($id)
    {
        return $this->forumAPI->SetThreadState($id, 'locked');
    }

    public function Seal($id)
    {
        return $this->forumAPI->SetThreadState($id, 'sealed');
    }

    public function Archive($id)
    {
        return $this->forumAPI->SetThreadState($id, 'archived');
    }

    public function Sticky($id)
    {
        return $this->forumAPI->SetThreadState($id, 'stickied');
    }

    public function Sanitise()
    {
        return $this->forumAPI->Sanitise();
    }

    public function MarkUnread($id)
    {
        return $this->forumAPI->MarkUnread($id);
    }
}
