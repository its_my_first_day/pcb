<?php

namespace App\Http\Controllers;

use App\Http\API\NewsAPI;
use Response;

class NewsController extends Controller
{
    private $newsAPI;
    public function __construct(NewsAPI $newsAPI)
    {
        $this->newsAPI = $newsAPI;
    }

    public function GetLatest()
    {
        return $this->newsAPI->GetLatest();
    }
}
