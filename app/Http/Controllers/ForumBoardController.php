<?php

namespace App\Http\Controllers;

use App\Http\API\ForumBoardAPI;
use Response;

class ForumBoardController extends Controller
{
    private $forumAPI;
    public function __construct(ForumBoardAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function GetAll()
    {
        return $this->forumAPI->GetBoardList();
    }

    public function Get($id)
    {
        return $this->forumAPI->GetBoard($id);
    }

    public function GetType($id)
    {
        return $this->forumAPI->GetBoardType($id);
    }

    public function Reorder($id)
    {
        return $this->forumAPI->Reorder($id);
    }
}
