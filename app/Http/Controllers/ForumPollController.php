<?php

namespace App\Http\Controllers;

use App\Http\API\ForumPollAPI;
use Response;

class ForumPollController extends Controller
{
    private $forumAPI;
    public function __construct(ForumPollAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function Create()
    {
        return $this->forumAPI->Create();
    }

    public function Vote($id)
    {
        return $this->forumAPI->Vote($id);
    }
}
