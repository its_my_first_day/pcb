<?php

namespace App\Http\Controllers;

use App\Http\Processors\Stats\ServerStats2;
use App\Http\Processors\Stats\ServerStats;
use App\Http\Processors\Stats\ServerStatsPlayerCount;
use App\Http\Requests;
use App\Http\Models\Server;
use Cache;
use Validator;
use Dingo\Api\Exception\ResourceException;
use Illuminate\Support\Facades\Input;
use Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ServerController extends Controller
{
    public function GetStatuses()
    {
        $servers = Server::all();

        // categorise by game
        $list = array();
        foreach($servers as $server)
        {
            $type = $server->type;
            if(!array_key_exists($type, $list))
            {
                $list[$type] = array();
            }

            // TODO: this needs to cache the default!
            Cache::get('server_status_' . $server->id, $server->LatestStatus);
            array_push($list[$type], $server);
        }

        return $list;
    }

    public function All()
    {
        $servers = Server::with('LatestStatus')
            ->get();

        // categorise by game
        $list = array();
        foreach($servers as $server)
        {
            $type = $server->type;
            if(!array_key_exists($type, $list))
            {
                $list[$type] = array();
            }

            array_push($list[$type], $server);
        }

        return $list;
    }

    public function Tag($id)
    {
        $tag = Input::get('tag');
        if($tag == null)
            throw new ResourceException('No tag specified.');

        $server = Server::find($id);
        if($server == null)
            throw new NotFoundHttpException('Server not found.');

        $whitelist = ['normal', 'hidden', 'offline', 'maintenance'];
        if(in_array($tag, $whitelist, true) === false)
            throw new BadRequestHttpException('Invalid tag.');

        $server->tag = $tag;
        $server->save();

        return [
            'success' => true
        ];
    }

    public function Save($id)
    {
        $name       = Input::get('name');
        $type       = Input::get('type');
        $ip         = Input::get('ip');
        $port       = Input::get('port');
        $show_port  = (bool)Input::get('show_port');
        $ip_alias   = Input::get('ip_alias');

        // validate input
        $validator = Validator::make([
            'name'      => $name,
            'type'      => $type,
            'ip'        => $ip,
            'show_port' => $show_port,
            'ip_alias'  => $ip_alias
        ], [
            'name'      => 'required|string',
            'type'      => 'required',
            'ip'        => 'required|ip',
            'show_port' => 'required|boolean',
            'ip_alias'  => 'min:3'
        ]);
        if($validator->fails())
            throw new ResourceException('Invalid form input.', $validator->errors());

        $server = Server::find($id);
        if($server == null)
            throw new NotFoundHttpException('Server not found.');

        $server->name = $name;
        $server->type = $type;
        $server->ip = $ip;
        $server->port = $port;
        $server->show_port = $show_port;

        if($ip_alias != null)
            $server->ip_alias = $ip_alias;
        else
            $server->ip_alias = null;

        $server->save();

        return [
            'success' => true
        ];
    }

    public function Delete($id)
    {
        $server = Server::find($id);
        if($server == null)
            throw new NotFoundHttpException('Server not found.');

        $server->delete();

        return [
            'success' => true
        ];
    }

    public function Create()
    {
        $name       = Input::get('name');
        $type       = Input::get('type');
        $ip         = Input::get('ip');
        $port       = Input::get('port');
        $show_port  = (bool)Input::get('show_port');
        $ip_alias   = Input::get('ip_alias');

        // validate input
        $validator = Validator::make([
            'name'      => $name,
            'type'      => $type,
            'ip'        => $ip,
            'show_port' => $show_port,
            'ip_alias'  => $ip_alias
        ], [
            'name'      => 'required|string',
            'type'      => 'required',
            'ip'        => 'required|ip',
            'show_port' => 'required|boolean',
            'ip_alias'  => 'min:3'
        ]);
        if($validator->fails())
            throw new ResourceException('Invalid form input.', $validator->errors());

        $data = [
            'name'      => $name,
            'type'      => strtolower($type),
            'ip'        => $ip,
            'show_port' => $show_port,
            'ip_alias'  => $ip_alias
        ];
        if($port != null)
            $data['port'] = $port;

        Server::create($data);

        return [
            'success' => true
        ];
    }

    /**
     * Returns all the server stats for the given server id
     *
     * @param $id
     * @return mixed
     */
    public function GetStats($id)
    {
        $stats = new ServerStats2();
        return Response::json($stats->Get($id));
    }

    public function GetPlayerCount($id, $interval)
    {
        $stats = new ServerStatsPlayerCount();
        return $stats->Get($id, null, $interval);
    }
}
