<?php

namespace App\Http\Controllers;

use App\Http\API\ForumCategoryAPI;
use Response;

class ForumCategoryController extends Controller
{
    private $forumAPI;
    public function __construct(ForumCategoryAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function Create()
    {
        return $this->forumAPI->Create();
    }

    public function Save($id)
    {
        return $this->forumAPI->Save($id);
    }

    public function Get($id)
    {
        return $this->forumAPI->Get($id);
    }

    public function All()
    {
        return $this->forumAPI->All();
    }

    public function Reorder($id)
    {
        return $this->forumAPI->Reorder($id);
    }

    public function Delete($id)
    {
        return $this->forumAPI->Delete($id);
    }
}
