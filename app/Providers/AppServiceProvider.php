<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \Illuminate\Database\Eloquent\Relations\Relation::morphMap([
            'post'          => \App\Http\Models\ForumPost::class,
            'announcement'  => \App\Http\Models\ForumAnnouncement::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
