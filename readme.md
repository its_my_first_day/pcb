# Project City Build

The newest iteration of www.projectcitybuild.com to be deployed when ready. The website is powered by Laravel 5, VueJS 1 and Bootstrap 3.

## Contributing

1. You will need to download the dependencies via Composer and Node Package Manager (NPM).

 * Using Command Prompt (or your CLI of choice), browse to the directory containing this repo's files. Run the following two commands:

 * Composer:
```
composer install
```

 * NPM:
```
npm install
```


2. Copy **.env.examples** to a new file, rename it to **.env** and fill out the missing connection details. Only your local database details are needed.

3. Open your CLI again and run:
```
php artisan migrate --seed
``` This will create the necessary tables in the database specified in **.env**.



### License

The website is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)