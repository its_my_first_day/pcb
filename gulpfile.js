var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir.config.sourcemaps = false;

elixir.config.js.browserify.plugins.push(
    {
        name: 'partition-bundle',
        options: {
            map: 'routes.json',
            output: './public/js',
            main: './resources/assets/js/app.js',
            url: 'js'
        }
    }
);

if (elixir.config.production) {
    elixir.config.js.browserify.plugins.push(
        {
            name: 'minifyify',
            options: {
                output: './public/js/test.map'
            }
        }
    );
}
elixir.config.js.browserify.options.debug = false;

elixir(function(mix) {

    // compile bootstrap and custom theme sass files
    mix.sass(['master.scss', 'bootstrap_override.scss'], 'public/css/master.css');

    mix.scripts([
        './resources/assets/libs/owl-carousel/owl.carousel.min.js',
        './resources/assets/libs/scripts.js'
    ], './public/js/scripts.js');

    // combine all stylesheets into one final css file
    mix.styles([
        './resources/assets/libs/bootstrap/bootstrap.min.css',
        './public/css/master.css',
        './resources/assets/libs/owl-carousel/owl.carousel.css'
    ], './public/css/master.css');

    mix.browserify('app.js');

});